import * as React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import MainTabNav from './app/routes/mainTabNav'
import store from './app/store/appStore'
import { Provider, observer } from 'mobx-react/native';
@observer
export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
      <Provider store={store}>
        <MainTabNav/>
        </Provider>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
