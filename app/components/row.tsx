import * as React from 'react';
import { Animated, StyleSheet, Text, View } from 'react-native';


export default class Row extends React.Component {
    itemAnimationValue = new Animated.Value(1) 
    constructor(props){
        super(props)
    }

    shouldComponentUpdate(nextProps):boolean{
       return (nextProps.item.name != this.props.item.name 
            || nextProps.item.highestBid != this.props.item.highestBid 
            || nextProps.item.last != this.props.item.last 
            || nextProps.item.percentChange != this.props.item.percentChange )
    }

    componentDidUpdate(){
        this.itemChangeAnimate()
    }

    itemChangeAnimate(){
        Animated.timing(
            this.itemAnimationValue, 
            {
              toValue: 0.2, 
            }
          ).start(()=>{
            Animated.timing(
                this.itemAnimationValue, 
                {
                  toValue: 1, 
                }
              ).start(); 
          }); 
    }

    render() {
        let item = this.props.item;
        let i = this.props.index;
        return (<View style={[
            
         i % 2 == 0 ? { backgroundColor: '#191616' } : null,
         
         ]}>
         <Animated.View style={[styles.item,{opacity: this.itemAnimationValue}]}>
            <Text style={styles.name}>
                {item.name}
            </Text>
            <Text style={styles.value}>
                {item.highestBid}
            </Text>
            <Text style={styles.value}>
                {item.last}
            </Text>
            <Text style={styles.value}>
                {item.percentChange}
            </Text>
            </Animated.View>
        </View>)

    }
}

const styles = StyleSheet.create({
    item: {
        paddingHorizontal: 10,
        height: 50,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    name: {
        color: '#fcc339',
        fontSize: 14,
    },
    value: {
        fontSize: 13,
        color: '#fff'
    },
});
