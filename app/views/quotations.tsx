import * as React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView
} from 'react-native';
import { inject, observer } from 'mobx-react/native';
import { BallIndicator } from 'react-native-indicators';
import Row from '../components/row'
import { Ionicons } from '@expo/vector-icons';
import { interval } from 'rxjs';
@inject('store')
@observer
export default class Quotations extends React.Component {
  private inverval: Observable<any>
  private didBlur;
  private didFocus;
  static navigationOptions = ({ navigation }) => ({
    tabBarIcon: ({ focused, tintColor }) => {
      return <Ionicons name="md-list" size={32} color={tintColor} />;
    },
    title: 'Котировки'
  })

  componentDidMount() {

    this.didBlur = this.props.navigation.addListener(
      'didBlur',
      () => {
        this.inverval.unsubscribe()
      }
    );
    this.didFocus = this.props.navigation.addListener(
      'didFocus',
      () => {
        this.inverval = interval(5000).subscribe(
          () => {
            this.props.store.getList()
          }
        );

      }
    );
  }


  componentWillUnmount() {
    this.didBlur.remove();
    this.didFocus.remove();
  }

  _renderList() {
    return <ScrollView>
      {this.props.store.list.map((item: object, i: number) =>
        <Row key={i} item={item} index={i} />
      )}
    </ScrollView>
  }

  render() {
    return (
      <View style={styles.container}>
        {this.props.store.error != null ? 
        <View style={{backgroundColor: 'red'}}>
          <Text style={styles.errorText}>ошибка</Text>
          </View> : null}

        <View style={styles.header}>
          <Text style={styles.headerTitle}>
            Name
        </Text>
          <Text style={styles.headerTitle}>
            Highest{'\n'}Bid
        </Text>
          <Text style={styles.headerTitle}>
            Last
        </Text>
          <Text style={styles.headerTitle}>
            Percent{'\n'}Change
        </Text>
        </View>
        <View style={styles.listContent}>
          {this.props.store.isLoad ?
            this._renderList() :
            <BallIndicator
              size={40}
              color='white' />}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop: 24,
  },
  header: {
    
    height: 60,
    backgroundColor: '#242120',
    width: '100%',
    paddingHorizontal: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 2,
    borderBottomColor: '#2e2b2a',
  },
  listContent: {
    backgroundColor: '#2e2b2a',
    flex: 1,
  },

  headerTitle: {
    fontWeight: 'bold',
    fontSize: 14,
    color: '#fff',
    textAlign: 'center'
  },
  errorText:{
    alignSelf: 'center',
    color: '#fff'
  }
});
