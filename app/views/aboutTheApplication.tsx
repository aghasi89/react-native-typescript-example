import * as React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

export default class AboutTheApplication extends React.Component {
  static  navigationOptions= ({ navigation }) => ({
    tabBarIcon: ({ focused, tintColor }) => {
      return <Ionicons name="md-information" size={32} color={tintColor} />;
    },
    title: 'О приложении'
  })

  render() {
    return (
      <View style={styles.container}>
        <Text>About The Application</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
