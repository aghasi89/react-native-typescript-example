import { createBottomTabNavigator } from 'react-navigation';
import AboutTheApplication from '../views/aboutTheApplication'
import Quotations from '../views/quotations'

const MainTabNav = createBottomTabNavigator({
    about: AboutTheApplication,
    quotations: Quotations
}, 
{
    tabBarOptions: {
       activeTintColor:'#fff',
      
        style: {
          backgroundColor: '#242120',
        },
      }
}
);

export default MainTabNav