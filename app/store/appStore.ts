import { observable, action } from 'mobx'

class Store {
    @observable list: Array<object> = []
    @observable isLoad: boolean = false
    @observable error:any = null
    @action async getList() {
        try {
            let data = await fetch(
                'https://poloniex.com/public?command=returnTicker',
                {
                    method: 'GET',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json'
                    },
                }
            )
            
            
            let result = await data.json()
            
            if(result['error']){
              //  this.list = []
                this.error = result['error']
                console.log(this.error);
            }
            else{
                this.error = null;
                this.list = Object.keys(result).map((key) => {
                    let r = {
                        name: key,
                        last: result[key].last,
                        highestBid: result[key].highestBid,
                        percentChange: result[key].percentChange
                    }
                    return r;
                })
            }
            this.isLoad = true;


        }
        catch (error) {
            console.log(error);

        }
    }
}

const store = new Store()
export default store